/* Copyright Statement:
 *
 * (C) 2005-2016  MediaTek Inc. All rights reserved.
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. ("MediaTek") and/or its licensors.
 * Without the prior written permission of MediaTek and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 * You may only use, reproduce, modify, or distribute (as applicable) MediaTek Software
 * if you have agreed to and been bound by the applicable license agreement with
 * MediaTek ("License Agreement") and been granted explicit permission to do so within
 * the License Agreement ("Permitted User").  If you are not a Permitted User,
 * please cease any access or use of MediaTek Software immediately.
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT MEDIATEK SOFTWARE RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES
 * ARE PROVIDED TO RECEIVER ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 */

#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include "wifi_api.h"
#include "syslog.h"
#include "lwip_network.h"
#include "network_init.h"
#include "lwip/netif.h"
#include "lwip/tcpip.h"
#include "lwip/sockets.h"
#include "ethernetif.h"
#include "lwip/sockets.h"
#include "netif/etharp.h"
#include "os.h"
#include "FreeRTOS.h"
#include "task.h"

#define CAMERA_TCP_SRV_PORT	 		6501
#define CAMERA_STACK_SIZE  			(1000/sizeof(portSTACK_TYPE))
#define CAMERA_TASK_PRIO   			2

bool g_wifi_init_ready = false;

void camera_thread(void *not_used)
{
    int s;
		int c = 0;
		int tcpRet =0;

    
    int rlen;
    struct sockaddr_in addr;
    char srv_buf[256] = {0};
		socklen_t sockaddr_len = 0;
    LOG_I(common, "camera_thread starts");
		
    while(1)
    {    
				os_memset(&addr, 0, sizeof(addr));
				addr.sin_len = sizeof(addr);
				addr.sin_family = AF_INET;
				addr.sin_port = lwip_htons(CAMERA_TCP_SRV_PORT);
				addr.sin_addr.s_addr = lwip_htonl(IPADDR_ANY);
				LOG_I(common,"Waiting Connection");
				
				/* Create the socket */
				s = lwip_socket(AF_INET, SOCK_STREAM, 0);
				if (s < 0) {
						LOG_I(common, "Camera server create failed");
						goto clean0;
				}

				tcpRet = lwip_bind(s, (struct sockaddr *)&addr, sizeof(addr));
				if (tcpRet < 0) {
						LOG_I(common, "Camera server bind failed");
						goto clean;
				}

				tcpRet = lwip_listen(s, 0);
				if (tcpRet < 0) {
						LOG_I(common, "Camera server listen failed");
						goto clean;
				}
    
        sockaddr_len = sizeof(addr);
        c = lwip_accept(s, (struct sockaddr *)&addr, &sockaddr_len);
        if (c < 0) {
            LOG_I(common, "Camera server accept error");
            goto clean;
        }

        LOG_I(common, "Camera server waiting for data...");
				tcpRet = 0;
				rlen = lwip_read(c, srv_buf, sizeof(srv_buf) - 1);
        while ( (tcpRet>=0) && (rlen>=0) ) 
				{
            if (rlen < 0) {
                LOG_I(common, "read error");
                break;
            }
            srv_buf[rlen] = 0; //for the next statement - printf string.
            LOG_I(common, "TCP server received data:%s", srv_buf);
								
						while(tcpRet>=0 && c>0)
						{
								tcpRet = lwip_write(c, srv_buf, rlen);      // sonar server
								
								if (tcpRet < 0) {
									LOG_I(common, "write error");
									//vTaskDelay(100);
								}
								else
								{
										LOG_I(common, "write successful %d", tcpRet);
								}
								
								vTaskDelay(1000);
						}				    
        }

        lwip_close(c);
				c = 0;
clean:	
				lwip_close(s);
clean0:	
				LOG_I(common,"End Connection");
    }
    
    LOG_I(common, "Camera server test completed");  
}

int32_t user_wifi_init_complete_handler(wifi_event_t event,
                                      uint8_t *payload,
                                      uint32_t length)
{
    LOG_I(common, "WiFi Init Done: port = %d", payload[6]);
    g_wifi_init_ready = true;


    if (pdPASS != xTaskCreate(camera_thread,
                              "camera_thread",
                              CAMERA_STACK_SIZE,
                              NULL,
                              CAMERA_TASK_PRIO,
                              NULL)) {
        LOG_I(common, "Cannot create camera_thread");
    }

    LOG_I(common, "Finish to create camera_thread");   
	
    return 1;
}

int32_t user_wifi_init_query_status(void)
{
    while (g_wifi_init_ready == false) {
        vTaskDelay(20);
    }
    return 1;
}

void user_wifi_app_entry(void *args)
{
    /* This is an example to show how to use wifi config APIs to change WiFI settings
     * If user not enable this code, system keep the wifi settings which user set by wifi_init()*/
#if 1
    uint8_t ssid[] = "MTK_AP";
    uint8_t password[] = "12345678";

    /*Wait until wifi initial done*/
    user_wifi_init_query_status();

    /*Change to AP mode*/
    wifi_set_opmode(WIFI_MODE_AP_ONLY);
    wifi_config_set_ssid(WIFI_PORT_AP, ssid, strlen(ssid));
    wifi_config_set_security_mode(WIFI_PORT_AP, WIFI_AUTH_MODE_WPA_PSK_WPA2_PSK, WIFI_ENCRYPT_TYPE_TKIP_AES_MIX);
    wifi_config_set_wpa_psk_key(WIFI_PORT_AP, password, strlen(password));
    wifi_config_set_channel(WIFI_PORT_AP, 6);
    wifi_config_reload_setting();
#endif

    while (1) {
        vTaskDelay(20);
    } 
}


