var group___mini_c_l_i =
[
    [ "cmd_s", "structcmd__s.html", [
      [ "cmd", "structcmd__s.html#a7353cae57e2530c316ddacb27ef14932", null ],
      [ "fptr", "structcmd__s.html#a4d3b5e2af853d4f6956db3024146a6b3", null ],
      [ "help", "structcmd__s.html#a67b7e976c6444c3d7fab151527bdae33", null ],
      [ "sub", "structcmd__s.html#a8804bdf6cab63a1879f85c3e144f92a7", null ]
    ] ],
    [ "cli_history_s", "structcli__history__s.html", [
      [ "full", "structcli__history__s.html#a2a50f126db5c13f768e597920b3cd1d6", null ],
      [ "history", "structcli__history__s.html#a5f9f02d4391a83cb01df97fe1ea2b6c5", null ],
      [ "history_max", "structcli__history__s.html#ae8e90525d227b25e857bc922acc4e645", null ],
      [ "index", "structcli__history__s.html#a852b86a2eaee9852ada7a43e61e311a2", null ],
      [ "input", "structcli__history__s.html#ab7a32d1060db5fb76b1bd8aa2abced98", null ],
      [ "line_max", "structcli__history__s.html#ac95ffafafd56d9f9e8cfecceb3a344ae", null ],
      [ "parse_token", "structcli__history__s.html#a8391e11aaa8d97c1cbbcfe6727e15405", null ],
      [ "position", "structcli__history__s.html#a6f14bde43ef12199f2d2ef07adb6c942", null ]
    ] ],
    [ "cli_s", "structcli__s.html", [
      [ "cmd", "structcli__s.html#a5f82bb55b80d469413234e036aa28d61", null ],
      [ "echo", "structcli__s.html#a156e99ac3bf4b0cdf63d4e58be6acfbb", null ],
      [ "get", "structcli__s.html#ad0833943e02bc23092e6b7033b005d8f", null ],
      [ "history", "structcli__s.html#ae50792c44a0c2348182a5f8b62b9b091", null ],
      [ "knock", "structcli__s.html#ada6619a36255731675a78de6d8dd825d", null ],
      [ "put", "structcli__s.html#a18e758911a527fb13f86a2dd757ca6f0", null ],
      [ "state", "structcli__s.html#a0b57aa10271a66f3dc936bba1d2f3830", null ],
      [ "tok", "structcli__s.html#a4dc0b8ca84189f04421d54845d1af057", null ]
    ] ],
    [ "CLI_MAX_TOKENS", "group___mini_c_l_i.html#ga19f580e6a89333793c51adcb65642b89", null ],
    [ "cli_history_t", "group___mini_c_l_i.html#gaac81ad0f7db6e9ecd0e1443d8102218c", null ],
    [ "cli_t", "group___mini_c_l_i.html#gad7f031df619c44fcc9dd2188fc0f36d4", null ],
    [ "cmd_t", "group___mini_c_l_i.html#ga4251cb68940eef7193936044e4db954f", null ],
    [ "fp_t", "group___mini_c_l_i.html#ga451ee6ac11300dd2e91f4416affca192", null ],
    [ "getch_fptr", "group___mini_c_l_i.html#ga47bb5a9751a5223a3bbad39f6a086c0d", null ],
    [ "knock_fptr", "group___mini_c_l_i.html#gaf0fb34283a8259b786bc20efbf6c295b", null ],
    [ "putch_fptr", "group___mini_c_l_i.html#gaea225940f0ef33dd8583e20e2c5cda6e", null ],
    [ "cli_hardcode_login", "group___mini_c_l_i.html#ga1f50eb3433ec4c0d542c7b91d520a63a", null ],
    [ "cli_init", "group___mini_c_l_i.html#ga1f91f952392fe3cfc182651622dec7e5", null ],
    [ "cli_line", "group___mini_c_l_i.html#ga0d797d1d1ebb75c889630f43e7cf3622", null ],
    [ "cli_logout", "group___mini_c_l_i.html#gab5520630388311dbec8ebe125c71b050", null ],
    [ "cli_task", "group___mini_c_l_i.html#ga73e8e71f9291413b84967adc3848d0dc", null ],
    [ "cli_tokens", "group___mini_c_l_i.html#ga9fdba4cca0642365768d42b4c60af757", null ]
];