var group__hal__irrx__struct =
[
    [ "hal_irrx_rc5_code_s", "structhal__irrx__rc5__code__s.html", [
      [ "bits", "structhal__irrx__rc5__code__s.html#a46a6da6b1936191571fd30b2a749f38c", null ],
      [ "code", "structhal__irrx__rc5__code__s.html#aa92b5897db548ac96cd40d8243041131", null ]
    ] ],
    [ "hal_irrx_pwd_config_s", "structhal__irrx__pwd__config__s.html", [
      [ "inverse", "structhal__irrx__pwd__config__s.html#acbd339ee0c9bb2890d56fcbf17d61978", null ],
      [ "terminate_threshold", "structhal__irrx__pwd__config__s.html#ae7eab56eb7c7d0a32beea7ed2b1a660e", null ]
    ] ],
    [ "hal_irrx_pwd_config_t", "group__hal__irrx__struct.html#ga211d682813a5010c64e1ddf4a4992210", null ],
    [ "hal_irrx_rc5_code_t", "group__hal__irrx__struct.html#gacd6cd7a9d5a46387dac18260e811c70d", null ]
];