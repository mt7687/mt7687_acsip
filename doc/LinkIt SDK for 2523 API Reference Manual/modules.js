var modules =
[
    [ "ATCI", "group___a_t_c_i.html", "group___a_t_c_i" ],
    [ "Battery Management", "group___battery__management___group.html", "group___battery__management___group" ],
    [ "FOTA", "group___f_o_t_a.html", "group___f_o_t_a" ],
    [ "HAL", "group___h_a_l.html", "group___h_a_l" ],
    [ "NVDM", "group___n_v_d_m.html", "group___n_v_d_m" ],
    [ "Sensor Subsystem", "group___sensor___subsystem___group.html", "group___sensor___subsystem___group" ]
];