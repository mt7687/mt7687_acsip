var group__hal__g2d__enum =
[
    [ "hal_g2d_codec_type_t", "group__hal__g2d__enum.html#ga142c0e01d9a30953833349ec6bd9d9fc", [
      [ "HAL_G2D_CODEC_TYPE_HW", "group__hal__g2d__enum.html#gga142c0e01d9a30953833349ec6bd9d9fcabe67e0f58a14146a739a25d67ed80e90", null ],
      [ "HAL_G2D_CODEC_TYPE_SW", "group__hal__g2d__enum.html#gga142c0e01d9a30953833349ec6bd9d9fca1aacd891aa1b149c323549255f599366", null ]
    ] ],
    [ "hal_g2d_color_format_t", "group__hal__g2d__enum.html#gabb8be4c58e8b4b867f87c92b46116064", [
      [ "HAL_G2D_COLOR_FORMAT_GRAY", "group__hal__g2d__enum.html#ggabb8be4c58e8b4b867f87c92b46116064af7da75267db90c28fe2c6152cfc1b9f5", null ],
      [ "HAL_G2D_COLOR_FORMAT_BGR888", "group__hal__g2d__enum.html#ggabb8be4c58e8b4b867f87c92b46116064adec71230674b36632e304b5244a8067f", null ],
      [ "HAL_G2D_COLOR_FORMAT_RGB565", "group__hal__g2d__enum.html#ggabb8be4c58e8b4b867f87c92b46116064ab076eb87d9cd9876475a9a6d713cd0df", null ],
      [ "HAL_G2D_COLOR_FORMAT_RGB888", "group__hal__g2d__enum.html#ggabb8be4c58e8b4b867f87c92b46116064a1980d1562f6977eeded8afba60b68c35", null ],
      [ "HAL_G2D_COLOR_FORMAT_ARGB8888", "group__hal__g2d__enum.html#ggabb8be4c58e8b4b867f87c92b46116064a8036931b042456ea7367fcb9fa5e6bf1", null ],
      [ "HAL_G2D_COLOR_FORMAT_PARGB8888", "group__hal__g2d__enum.html#ggabb8be4c58e8b4b867f87c92b46116064adb41a6e7daefa0c7ff2e9cec2bed2051", null ],
      [ "HAL_G2D_COLOR_FORMAT_ARGB8565", "group__hal__g2d__enum.html#ggabb8be4c58e8b4b867f87c92b46116064a9ac2efc74d64c850965da73c84bbbee7", null ],
      [ "HAL_G2D_COLOR_FORMAT_ARGB6666", "group__hal__g2d__enum.html#ggabb8be4c58e8b4b867f87c92b46116064a87602fe49261b57b7381a6b1fec21bb1", null ],
      [ "HAL_G2D_COLOR_FORMAT_PARGB8565", "group__hal__g2d__enum.html#ggabb8be4c58e8b4b867f87c92b46116064a13e85225ef14fc80a7a9bddf35f1f13d", null ],
      [ "HAL_G2D_COLOR_FORMAT_PARGB6666", "group__hal__g2d__enum.html#ggabb8be4c58e8b4b867f87c92b46116064a9077b0584b6d36205ff0b03498f33120", null ],
      [ "HAL_G2D_COLOR_FORMAT_UYVY422", "group__hal__g2d__enum.html#ggabb8be4c58e8b4b867f87c92b46116064a1a53b390463254b89c223df167bd3896", null ],
      [ "HAL_G2D_COLOR_FORMAT_ARGB8888_INDEX_COLOR", "group__hal__g2d__enum.html#ggabb8be4c58e8b4b867f87c92b46116064a636b3a8453e7d07cceb2c194cc93dd61", null ],
      [ "HAL_G2D_COLOR_FORMAT_ARGB4444", "group__hal__g2d__enum.html#ggabb8be4c58e8b4b867f87c92b46116064ab3b83b6f58f5a006442033a525b5c402", null ]
    ] ],
    [ "hal_g2d_get_handle_mode_t", "group__hal__g2d__enum.html#ga3500ae950ae0679737d43732c0bed76b", [
      [ "HAL_G2D_GET_HANDLE_MODE_BLOCKING", "group__hal__g2d__enum.html#gga3500ae950ae0679737d43732c0bed76bacd88717aee67619ce6638ad19f3691a5", null ],
      [ "HAL_G2D_GET_HANDLE_MODE_NON_BLOCKING", "group__hal__g2d__enum.html#gga3500ae950ae0679737d43732c0bed76ba5dcd4c0d022ce37911c1c32e2bb14784", null ]
    ] ],
    [ "hal_g2d_overlay_layer_function_t", "group__hal__g2d__enum.html#ga18aa1309e28245fdac99fc6bbe0dc777", [
      [ "HAL_G2D_OVERLAY_LAYER_FUNCTION_NORMAL_FONT", "group__hal__g2d__enum.html#gga18aa1309e28245fdac99fc6bbe0dc777aee69c0bf661627b71caeecff3909fde7", null ],
      [ "HAL_G2D_OVERLAY_LAYER_FUNCTION_AA_FONT", "group__hal__g2d__enum.html#gga18aa1309e28245fdac99fc6bbe0dc777af9ae5bb2363de765eec1e69f46333a53", null ],
      [ "HAL_G2D_OVERLAY_LAYER_FUNCTION_RECTFILL", "group__hal__g2d__enum.html#gga18aa1309e28245fdac99fc6bbe0dc777aead5c81cf6a2da376a29a03ad9e9a634", null ],
      [ "HAL_G2D_OVERLAY_LAYER_FUNCTION_BUFFER", "group__hal__g2d__enum.html#gga18aa1309e28245fdac99fc6bbe0dc777ac74e12f14b2a49a8ef6869a65e7f6f2f", null ]
    ] ],
    [ "hal_g2d_overlay_layer_t", "group__hal__g2d__enum.html#ga086b9324b7ba3e2b6a637fa3e5dc4e7d", [
      [ "HAL_G2D_OVERLAY_LAYER0", "group__hal__g2d__enum.html#gga086b9324b7ba3e2b6a637fa3e5dc4e7da5caf7b92a322e4d5e2a564bbd7a76bdc", null ],
      [ "HAL_G2D_OVERLAY_LAYER1", "group__hal__g2d__enum.html#gga086b9324b7ba3e2b6a637fa3e5dc4e7daea3f942be8cec3fcbb8b9fd2aa04fc08", null ],
      [ "HAL_G2D_OVERLAY_LAYER2", "group__hal__g2d__enum.html#gga086b9324b7ba3e2b6a637fa3e5dc4e7daf92612d2690093a520166649162d8c0a", null ],
      [ "HAL_G2D_OVERLAY_LAYER3", "group__hal__g2d__enum.html#gga086b9324b7ba3e2b6a637fa3e5dc4e7da264fafa0529a83dc862d6edf13abcff2", null ]
    ] ],
    [ "hal_g2d_rotate_angle_t", "group__hal__g2d__enum.html#gab0a3b3c2de63fd28eb868af8eefcb3e8", [
      [ "HAL_G2D_ROTATE_ANGLE_0", "group__hal__g2d__enum.html#ggab0a3b3c2de63fd28eb868af8eefcb3e8aeadbf3ec9a1bcbf3fec288b4f8e98424", null ],
      [ "HAL_G2D_ROTATE_ANGLE_90", "group__hal__g2d__enum.html#ggab0a3b3c2de63fd28eb868af8eefcb3e8ac8cf0c1ee588c5e138fcee23f77780c4", null ],
      [ "HAL_G2D_ROTATE_ANGLE_180", "group__hal__g2d__enum.html#ggab0a3b3c2de63fd28eb868af8eefcb3e8a57f405bbe42dd6d0e4cee2509bd5bd3e", null ],
      [ "HAL_G2D_ROTATE_ANGLE_270", "group__hal__g2d__enum.html#ggab0a3b3c2de63fd28eb868af8eefcb3e8afa780981f6fcabb9f8b7f68ef1d839cb", null ],
      [ "HAL_G2D_ROTATE_ANGLE_MIRROR_0", "group__hal__g2d__enum.html#ggab0a3b3c2de63fd28eb868af8eefcb3e8a9573e7e6181d33232bdbf8ee42481f5e", null ],
      [ "HAL_G2D_ROTATE_ANGLE_MIRROR_90", "group__hal__g2d__enum.html#ggab0a3b3c2de63fd28eb868af8eefcb3e8af96d43d1ac08b6e78e55809dbe8e591e", null ],
      [ "HAL_G2D_ROTATE_ANGLE_MIRROR_180", "group__hal__g2d__enum.html#ggab0a3b3c2de63fd28eb868af8eefcb3e8a2fc19b0006f8108bca77e215c0769dc2", null ],
      [ "HAL_G2D_ROTATE_ANGLE_MIRROR_270", "group__hal__g2d__enum.html#ggab0a3b3c2de63fd28eb868af8eefcb3e8af3c663778129422ecb07de18cfc5194d", null ]
    ] ],
    [ "hal_g2d_status_t", "group__hal__g2d__enum.html#ga478e50300d254786bbcc775d7e0951cb", [
      [ "HAL_G2D_STATUS_HW_ERROR", "group__hal__g2d__enum.html#gga478e50300d254786bbcc775d7e0951cba938c7b7bb88873d2bff147ebf79579b4", null ],
      [ "HAL_G2D_STATUS_NOT_SUPPORTEDED_COLOR_FORMAT", "group__hal__g2d__enum.html#gga478e50300d254786bbcc775d7e0951cba7b333f004850aec4a31f8ea6598cf6cb", null ],
      [ "HAL_G2D_STATUS_INVALID_PARAMETER", "group__hal__g2d__enum.html#gga478e50300d254786bbcc775d7e0951cbac0c86a0dc22a79fa6304ccb0d5d52286", null ],
      [ "HAL_G2D_STATUS_NOT_SUPPORTED", "group__hal__g2d__enum.html#gga478e50300d254786bbcc775d7e0951cba73fe179f546092625caf1db720c4d90f", null ],
      [ "HAL_G2D_STATUS_BUSY", "group__hal__g2d__enum.html#gga478e50300d254786bbcc775d7e0951cba1eb00133c3f34f968d897ebd06a8ad6d", null ],
      [ "HAL_G2D_STATUS_OK", "group__hal__g2d__enum.html#gga478e50300d254786bbcc775d7e0951cba7fbb5f4a047f38486174a112e164d76e", null ]
    ] ],
    [ "hal_g2d_supported_function_t", "group__hal__g2d__enum.html#ga2a4702f65778b424f5772f2d8871131d", [
      [ "HAL_G2D_SUPPORTED_FUNCTION_BITBLT", "group__hal__g2d__enum.html#gga2a4702f65778b424f5772f2d8871131da1834a93d790f91e893bc86a69170ab45", null ],
      [ "HAL_G2D_SUPPORTED_FUNCTION_RECTFILL", "group__hal__g2d__enum.html#gga2a4702f65778b424f5772f2d8871131da4ed207ea403478dc0f2fc29ab2bf16d4", null ],
      [ "HAL_G2D_SUPPORTED_FUNCTION_NORMAL_FONT", "group__hal__g2d__enum.html#gga2a4702f65778b424f5772f2d8871131da28a34afa2922836964d0cf9bcba3bd33", null ],
      [ "HAL_G2D_SUPPORTED_FUNCTION_AA_FONT", "group__hal__g2d__enum.html#gga2a4702f65778b424f5772f2d8871131da7f58b33e232e48ca0237a2800aa14eac", null ],
      [ "HAL_G2D_SUPPORTED_FUNCTION_TILT_FONT", "group__hal__g2d__enum.html#gga2a4702f65778b424f5772f2d8871131da248fface117957ad879af2c897d1e90a", null ],
      [ "HAL_G2D_SUPPORTED_FUNCTION_LT", "group__hal__g2d__enum.html#gga2a4702f65778b424f5772f2d8871131da88775e7831340677f6bea6e614955770", null ],
      [ "HAL_G2D_SUPPORTED_FUNCTION_SAD", "group__hal__g2d__enum.html#gga2a4702f65778b424f5772f2d8871131daa306f9402eb0a351fe962520c5e9c258", null ],
      [ "HAL_G2D_SUPPORTED_FUNCTION_OVERLAY", "group__hal__g2d__enum.html#gga2a4702f65778b424f5772f2d8871131daa021d60cb0fa23dd41c13ebf9d3a4fde", null ],
      [ "HAL_G2D_SUPPORTED_FUNCTION_DITHERING", "group__hal__g2d__enum.html#gga2a4702f65778b424f5772f2d8871131daa42a75a12bcbc3900df72022086d4b77", null ]
    ] ]
];