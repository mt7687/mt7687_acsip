var searchData=
[
  ['sensor_5factivity_5ftype_5ft',['sensor_activity_type_t',['../group__sensor__fusion__algorithm__enum.html#ga84ab1a4a675923492b201f22e26ab91d',1,'sensor_alg_interface.h']]],
  ['sensor_5ffall_5ftype_5ft',['sensor_fall_type_t',['../group__sensor__fusion__algorithm__enum.html#gad87f2d0d0d2c6b1ac166372324d43b58',1,'sensor_alg_interface.h']]],
  ['sensor_5fgesture_5ftype_5ft',['sensor_gesture_type_t',['../group__sensor__fusion__algorithm__enum.html#ga0a4993ea5898641aa426fd35ad1e95d7',1,'sensor_alg_interface.h']]],
  ['sensor_5freport_5fmode_5ft',['sensor_report_mode_t',['../group__sensor__fusion__algorithm__enum.html#gaa0d4316dc01201f038501ceb45f00e06',1,'sensor_alg_interface.h']]],
  ['sensor_5fstatus_5ft',['sensor_status_t',['../group__sensor__fusion__algorithm__enum.html#ga0a5ac5472e42c20c0c1aba197ef67a70',1,'sensor_alg_interface.h']]]
];
