var searchData=
[
  ['baudrate',['baudrate',['../structhal__uart__config__t.html#a4326827f3f05aba7eae095213c9d9d14',1,'hal_uart_config_t']]],
  ['beatpos',['beatPos',['../structsensor__heart__rate__variability__event__t.html#a1ce4aebbdbf181baf55cc9a8afb606ac',1,'sensor_heart_rate_variability_event_t']]],
  ['bias',['bias',['../structsensor__uncalibrated__event__t.html#a6cf7a7c815c2d88bf7251df4dfa28a4a',1,'sensor_uncalibrated_event_t']]],
  ['bio_5fdata',['bio_data',['../structsensor__data__unit__t.html#a27f767b78d4530a7b0b0e809755e6558',1,'sensor_data_unit_t']]],
  ['bit_5forder',['bit_order',['../structhal__spi__master__config__t.html#a5d94c69f3e000c69002ef7f409330109',1,'hal_spi_master_config_t::bit_order()'],['../structhal__spi__slave__config__t.html#aa095791cb53c22ff6e819f30ae8cde9d',1,'hal_spi_slave_config_t::bit_order()']]],
  ['block',['block',['../structhal__sdio__command53__config__t.html#a3d9514fffe0e0473d1c4f36184d394ef',1,'hal_sdio_command53_config_t']]],
  ['blood_5fpressure_5ft',['blood_pressure_t',['../structsensor__data__unit__t.html#aeb6ac973634171f36f268f63fb0fd04e',1,'sensor_data_unit_t']]],
  ['bound',['bound',['../structhal__display__color__sharpness__t.html#aed4c5fb1909f1a8456b619db6d399a61',1,'hal_display_color_sharpness_t']]],
  ['bpm',['bpm',['../structsensor__heart__rate__event__t.html#ad7885d0db3e75621973ca8659977fa6e',1,'sensor_heart_rate_event_t']]],
  ['buffer',['buffer',['../structhal__sdio__command53__config__t.html#ac361fef6bf135953d536299eb0b119c3',1,'hal_sdio_command53_config_t']]],
  ['buffer_5faddress',['buffer_address',['../structhal__display__lcd__layer__input__t.html#a829845f140156f1f1b35db7a2d5b3b27',1,'hal_display_lcd_layer_input_t']]],
  ['bus_5fwidth',['bus_width',['../structhal__sd__config__t.html#a06d9587c82f4ea63d5bc5db09c133e9d',1,'hal_sd_config_t::bus_width()'],['../structhal__sdio__config__t.html#a711672df53c56f2317d3e71c9da3aeed',1,'hal_sdio_config_t::bus_width()']]],
  ['bypass_5fconfig',['bypass_config',['../structhal__display__color__parameter__t.html#ac8609d2e6bf61e68632cffb0bbec794a',1,'hal_display_color_parameter_t']]],
  ['byte_5forder',['byte_order',['../structhal__spi__master__advanced__config__t.html#a75a833b37a0a46791ed14491d3957790',1,'hal_spi_master_advanced_config_t']]],
  ['byte_5fswap_5fflag',['byte_swap_flag',['../structhal__display__lcd__layer__input__t.html#ae2b302dbe4fa0ddabee7f2f84aa297e1',1,'hal_display_lcd_layer_input_t']]],
  ['byte_5fswitch',['byte_switch',['../structhal__display__lcd__interface__start__byte__mode__t.html#aef282edb7c90920f90165eb8ae3f85ac',1,'hal_display_lcd_interface_start_byte_mode_t']]]
];
