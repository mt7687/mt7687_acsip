var searchData=
[
  ['ta_5fget',['ta_get',['../structhal__display__dsi__dphy__timing__struct__t.html#ae8065d559b12611db639f20dfc8c822a',1,'hal_display_dsi_dphy_timing_struct_t']]],
  ['ta_5fgo',['ta_go',['../structhal__display__dsi__dphy__timing__struct__t.html#ab6f16b8add9743bd82114ee34b94fd5a',1,'hal_display_dsi_dphy_timing_struct_t']]],
  ['ta_5fsure',['ta_sure',['../structhal__display__dsi__dphy__timing__struct__t.html#ad489bb6f6ab42331f02d29c14fe1ba60',1,'hal_display_dsi_dphy_timing_struct_t']]],
  ['target_5fend_5fx',['target_end_x',['../structhal__display__lcd__roi__output__t.html#a5241990e79d7cf42a952137f4c3d33ec',1,'hal_display_lcd_roi_output_t']]],
  ['target_5fend_5fy',['target_end_y',['../structhal__display__lcd__roi__output__t.html#a4160afc1ac655227337293ac0a198218',1,'hal_display_lcd_roi_output_t']]],
  ['target_5fstart_5fx',['target_start_x',['../structhal__display__lcd__roi__output__t.html#a5e3a0dd897da390bd986bec357a4f871',1,'hal_display_lcd_roi_output_t']]],
  ['target_5fstart_5fy',['target_start_y',['../structhal__display__lcd__roi__output__t.html#a33ca840070221fa94b85b30ae03b24ef',1,'hal_display_lcd_roi_output_t']]],
  ['three_5fwire_5fmode',['three_wire_mode',['../structhal__display__lcd__interface__mode__t.html#acb98cd09d3841362043bcc00a5abfcee',1,'hal_display_lcd_interface_mode_t']]],
  ['time_5fstamp',['time_stamp',['../structsensor__data__unit__t.html#ab42b0f83e39af1af41a8bf2bd652dd89',1,'sensor_data_unit_t']]],
  ['timeout_5fthreshold',['timeout_threshold',['../structhal__spi__slave__config__t.html#aea9c7a0f0dcff7e5fb5c4b2829e807df',1,'hal_spi_slave_config_t']]],
  ['trigger_5fmode',['trigger_mode',['../structhal__eint__config__t.html#accaa73cf4b560bd28e45430dae8346d4',1,'hal_eint_config_t']]],
  ['two_5fdata_5fflag',['two_data_flag',['../structhal__display__lcd__interface__2__data__lane__t.html#a3093d75e34a987974db8cd74bbbb2338',1,'hal_display_lcd_interface_2_data_lane_t']]],
  ['two_5fdata_5fwidth',['two_data_width',['../structhal__display__lcd__interface__2__data__lane__t.html#a5121d889515aefc8c7259568c9607f1e',1,'hal_display_lcd_interface_2_data_lane_t']]],
  ['type',['type',['../structsensor__bio__t.html#a449e574ed6911881dc55507cb5635c2c',1,'sensor_bio_t::type()'],['../structsensor__subscriber.html#ad44b615021ed3ccb734fcaf583ef4a03',1,'sensor_subscriber::type()']]]
];
