var searchData=
[
  ['accdet_5fhook_5fkey_5fdebounce',['accdet_hook_key_debounce',['../structhal__accdet__debounce__time__t.html#ae5930fc96084ebe9e645355c5257fb68',1,'hal_accdet_debounce_time_t']]],
  ['accdet_5fplug_5fin_5fdebounce',['accdet_plug_in_debounce',['../structhal__accdet__debounce__time__t.html#aca4203602518a14e3ac75d9acdcde7c9',1,'hal_accdet_debounce_time_t']]],
  ['accdet_5fplug_5fout_5fdebounce',['accdet_plug_out_debounce',['../structhal__accdet__debounce__time__t.html#a545612853289628d717ae52293c5940f',1,'hal_accdet_debounce_time_t']]],
  ['accelerometer_5ft',['accelerometer_t',['../structsensor__data__unit__t.html#a3892547e9c797739ef137b261e2c9843',1,'sensor_data_unit_t']]],
  ['accumulate',['accumulate',['../structsensor__descriptor.html#a4120b49780882cb46f0da7fcbfd3d94b',1,'sensor_descriptor']]],
  ['accumulated_5fstep_5fcount',['accumulated_step_count',['../structsensor__pedometer__event__t.html#a9ec7c63e8d022963ef41ed966da48a7a',1,'sensor_pedometer_event_t']]],
  ['accumulated_5fstep_5flength',['accumulated_step_length',['../structsensor__pedometer__event__t.html#a521f95fb4cfa805ccde01bb6160bec7d',1,'sensor_pedometer_event_t']]],
  ['activity_5fdata_5ft',['activity_data_t',['../structsensor__data__unit__t.html#a7b176cc7381dfe976b9d29f925cdbb6c',1,'sensor_data_unit_t']]],
  ['address',['address',['../structhal__sdio__command53__config__t.html#ac0d31ca829f934cccd89f8054e02773e',1,'hal_sdio_command53_config_t::address()'],['../structhal__sdio__command52__config__t.html#ac0d31ca829f934cccd89f8054e02773e',1,'hal_sdio_command52_config_t::address()']]],
  ['alpha',['alpha',['../structhal__display__lcd__layer__input__t.html#acf5bb76caa419c0fa231dbd66c881084',1,'hal_display_lcd_layer_input_t']]],
  ['alpha_5fflag',['alpha_flag',['../structhal__display__lcd__layer__input__t.html#af736a1e7aab8221a164963f73eb79d82',1,'hal_display_lcd_layer_input_t']]],
  ['analog_5fgain_5findex',['analog_gain_index',['../structhal__audio__stream__mode__t.html#adea90db103c9c36ff8c690bf51d2f929',1,'hal_audio_stream_mode_t']]],
  ['audio_5fdevice',['audio_device',['../structhal__audio__stream__mode__t.html#aa0c4c053963a5a387938b75f2fdbb09f',1,'hal_audio_stream_mode_t']]],
  ['audio_5fpath_5ftype',['audio_path_type',['../structhal__audio__stream__t.html#aa39953989ead66d2fe7e012a588042f5',1,'hal_audio_stream_t']]],
  ['azimuth',['azimuth',['../structsensor__vec__t.html#aec6ccb30c3ffcd0418d53f6ca60a5518',1,'sensor_vec_t']]]
];
