var searchData=
[
  ['sensor_5fdata_5ft',['sensor_data_t',['../group__sensor__fusion__algorithm__struct.html#gaad1f1e51b197f3af300e8a014abae695',1,'sensor_alg_interface.h']]],
  ['sensor_5fdescriptor_5fget_5fresult_5fcallback_5ft',['sensor_descriptor_get_result_callback_t',['../group__sensor__fusion__algorithm__typedef.html#ga3187e29c695950c0fc02fafc7001e323',1,'sensor_alg_interface.h']]],
  ['sensor_5fdescriptor_5foperate_5fcallback_5ft',['sensor_descriptor_operate_callback_t',['../group__sensor__fusion__algorithm__typedef.html#ga520486383f50f788f018c0710774c235',1,'sensor_alg_interface.h']]],
  ['sensor_5fdescriptor_5fprocess_5fdata_5fcallback_5ft',['sensor_descriptor_process_data_callback_t',['../group__sensor__fusion__algorithm__typedef.html#gac38a925eb67580204a36264dc59e2377',1,'sensor_alg_interface.h']]],
  ['sensor_5fdescriptor_5ft',['sensor_descriptor_t',['../group__sensor__fusion__algorithm__struct.html#gab3153c83997df0464531e4759b721ef8',1,'sensor_alg_interface.h']]],
  ['sensor_5fdriver_5fobject_5foperate_5fcallback_5ft',['sensor_driver_object_operate_callback_t',['../group__sensor__driver__typedef.html#gaee8ebe4d153f2b8f8894a5cf58dc0b4c',1,'sensor_alg_interface.h']]],
  ['sensor_5fdriver_5fobject_5ft',['sensor_driver_object_t',['../group__sensor__driver__struct.html#gab4b317fc7158a9c4d26dfa1c6811aee0',1,'sensor_alg_interface.h']]],
  ['sensor_5finput_5flist_5ft',['sensor_input_list_t',['../group__sensor__fusion__algorithm__struct.html#ga6c5fb6356215a71083675d3a194c8efc',1,'sensor_alg_interface.h']]],
  ['sensor_5fsubscriber_5fsend_5fdigest_5fcallback_5ft',['sensor_subscriber_send_digest_callback_t',['../group__sensor__app__typedef.html#ga9dbab461f565db999e88511b9477a5eb',1,'sensor_alg_interface.h']]],
  ['sensor_5fsubscriber_5ft',['sensor_subscriber_t',['../group__sensor__app__struct.html#gac878f15c0e254a7344e4b5d82c1b378d',1,'sensor_alg_interface.h']]]
];
