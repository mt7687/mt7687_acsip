var searchData=
[
  ['sensor_5fdriver_5fattach',['sensor_driver_attach',['../group__sensor__driver__interface.html#ga20766f503ba9ad5a3dbb714abaeb3c52',1,'sensor_alg_interface.h']]],
  ['sensor_5ffusion_5falgorithm_5fnotify',['sensor_fusion_algorithm_notify',['../group__sensor__fusion__algorithm__interface.html#gadca1e4a979e195f54a2500d2b2414dde',1,'sensor_alg_interface.h']]],
  ['sensor_5ffusion_5falgorithm_5fregister_5fdata_5fbuffer',['sensor_fusion_algorithm_register_data_buffer',['../group__sensor__fusion__algorithm__interface.html#ga48af00fbd807d74fd4d709bf0dd0851e',1,'sensor_alg_interface.h']]],
  ['sensor_5ffusion_5falgorithm_5fregister_5ftype',['sensor_fusion_algorithm_register_type',['../group__sensor__fusion__algorithm__interface.html#ga840f9ad6e1e5039eb244dc2429eb7a25',1,'sensor_alg_interface.h']]],
  ['sensor_5fget_5flatest_5fsensor_5fdata',['sensor_get_latest_sensor_data',['../group__sensor__app__interface.html#gaf65a85c873d24d4832605553a3693449',1,'sensor_alg_interface.h']]],
  ['sensor_5fsubscribe_5fsensor',['sensor_subscribe_sensor',['../group__sensor__app__interface.html#ga632e05d37fb574ee917d95418b1ed23d',1,'sensor_alg_interface.h']]],
  ['sensor_5funsubscribe_5fsensor',['sensor_unsubscribe_sensor',['../group__sensor__app__interface.html#ga3a74acccb09b1330d9f6680392d73fc6',1,'sensor_alg_interface.h']]]
];
