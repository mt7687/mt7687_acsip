var searchData=
[
  ['layer_5fenable',['layer_enable',['../structhal__display__lcd__layer__input__t.html#afd171341f8349abda2d5f4171e067bd8',1,'hal_display_lcd_layer_input_t']]],
  ['lf',['LF',['../structsensor__heart__rate__variability__event__t.html#a09c0bf68b2941f9d3d3c9d66dcf5f3b0',1,'sensor_heart_rate_variability_event_t']]],
  ['lf_5fhf',['LF_HF',['../structsensor__heart__rate__variability__event__t.html#abf54019b9c5fef54da600d57647e6bda',1,'sensor_heart_rate_variability_event_t']]],
  ['light',['light',['../structsensor__data__unit__t.html#a0c566a121f4adb4079774876058bf672',1,'sensor_data_unit_t']]],
  ['lighter_5fto_5fdarker_5ftime1',['lighter_to_darker_time1',['../structhal__isink__breath__mode__t.html#ae8401365e344350ecf073f8599fced13',1,'hal_isink_breath_mode_t']]],
  ['lighter_5fto_5fdarker_5ftime2',['lighter_to_darker_time2',['../structhal__isink__breath__mode__t.html#a26ca79a25e576186be7faa788e45ee16',1,'hal_isink_breath_mode_t']]],
  ['lightest_5ftime',['lightest_time',['../structhal__isink__breath__mode__t.html#acab2fc32b6b914775f2a77df1152fb70',1,'hal_isink_breath_mode_t']]],
  ['limit',['limit',['../structhal__display__color__sharpness__t.html#a36b9e8087257b4caf4d993fca15db087',1,'hal_display_color_sharpness_t']]],
  ['longpress_5ftime',['longpress_time',['../structhal__keypad__config__t.html#ab4b5eefe5cb9873d1c3a2756f14653a7',1,'hal_keypad_config_t']]],
  ['loopback',['loopback',['../structhal__audio__stream__t.html#a3ea0b2f2695b0c84cd8495484456a58e',1,'hal_audio_stream_t']]],
  ['lpx',['lpx',['../structhal__display__dsi__dphy__timing__struct__t.html#a5be160bc1438f5c266605cad5b0be54f',1,'hal_display_dsi_dphy_timing_struct_t']]]
];
