var searchData=
[
  ['gain',['gain',['../structhal__display__color__sharpness__t.html#a5c9c0f59d06b7360c990c3f49be81fa7',1,'hal_display_color_sharpness_t::gain()'],['../structhal__display__color__saturation__t.html#a5c9c0f59d06b7360c990c3f49be81fa7',1,'hal_display_color_saturation_t::gain()']]],
  ['gain0',['gain0',['../structhal__display__color__contrast__t.html#a33b06ad71948cb43a54cc18aaa6b4a8a',1,'hal_display_color_contrast_t']]],
  ['gain1',['gain1',['../structhal__display__color__contrast__t.html#a1171be1575c0738f9defecdcad40aa64',1,'hal_display_color_contrast_t']]],
  ['gain2',['gain2',['../structhal__display__color__contrast__t.html#a096bdfb0edee0af042c71654eb549ab4',1,'hal_display_color_contrast_t']]],
  ['gesture_5fdata_5ft',['gesture_data_t',['../structsensor__data__unit__t.html#a9f46fe1289580ea521c476979ef02771',1,'sensor_data_unit_t']]],
  ['get_5fresult',['get_result',['../structsensor__descriptor.html#ad418f4389362060320cd9f786417cc3f',1,'sensor_descriptor']]],
  ['get_5ftick',['get_tick',['../structhal__spi__master__advanced__config__t.html#a856ea9088de23d730629c55b4c76d3c7',1,'hal_spi_master_advanced_config_t']]],
  ['gyroscope_5ft',['gyroscope_t',['../structsensor__data__unit__t.html#ae93fe0dca1e58a9ac21ae1652774ac05',1,'sensor_data_unit_t']]]
];
