var structsensor__heart__rate__variability__event__t =
[
    [ "beatPos", "structsensor__heart__rate__variability__event__t.html#a1ce4aebbdbf181baf55cc9a8afb606ac", null ],
    [ "HF", "structsensor__heart__rate__variability__event__t.html#a72a8cfaac70dc4f2e87a66491dfa054d", null ],
    [ "LF", "structsensor__heart__rate__variability__event__t.html#a09c0bf68b2941f9d3d3c9d66dcf5f3b0", null ],
    [ "LF_HF", "structsensor__heart__rate__variability__event__t.html#abf54019b9c5fef54da600d57647e6bda", null ],
    [ "numBeatPos", "structsensor__heart__rate__variability__event__t.html#a5b0343cc5c9494b1df3e419fc57b97cd", null ],
    [ "SDNN", "structsensor__heart__rate__variability__event__t.html#a79dbf2f1c51bdf7b337fec54851bbff2", null ]
];