var struct_network =
[
    [ "cacertl", "struct_network.html#a3ee1ca3334d0586d0d68b7f16fe4469e", null ],
    [ "clicert", "struct_network.html#aa1e1180ffe6580bfa9449c323e39ee3b", null ],
    [ "conf", "struct_network.html#ae33197eb1335088e268c9b5f7927cd96", null ],
    [ "disconnect", "struct_network.html#af5ded5fcb593520fee26c7c9ce8d5954", null ],
    [ "fd", "struct_network.html#a420529def6c8841656e7472341e1d001", null ],
    [ "mqttread", "struct_network.html#a4b1586f1b679bf026d78359f85a9036a", null ],
    [ "mqttwrite", "struct_network.html#a688347455fb90282557e729d996db8bc", null ],
    [ "my_socket", "struct_network.html#aa4855f00a2b606ce9747c724b37d80ca", null ],
    [ "pkey", "struct_network.html#aa19ededa9af3083e983832a75167cb01", null ],
    [ "ssl", "struct_network.html#ab63c630047c7f8971de74898e2fc0dee", null ]
];