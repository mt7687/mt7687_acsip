var group___http_client =
[
    [ "Define", "group__httpclient__define.html", "group__httpclient__define" ],
    [ "Enum", "group__httpclient__enum.html", "group__httpclient__enum" ],
    [ "Struct", "group__httpclient__struct.html", "group__httpclient__struct" ],
    [ "httpclient_close", "group___http_client.html#ga30e0d9e2e2e17cb494b5bec02feffa4b", null ],
    [ "httpclient_connect", "group___http_client.html#gabcd08f67f4af4a6bd904b3628b7ee065", null ],
    [ "httpclient_delete", "group___http_client.html#ga418a7298e3322f247a85fb0e1996e0b5", null ],
    [ "httpclient_get", "group___http_client.html#ga4e7fd7f8c70ac94dd814560e5b0bdc35", null ],
    [ "httpclient_get_response_code", "group___http_client.html#ga65884d837ed6815b8f83a2138cd4211e", null ],
    [ "httpclient_post", "group___http_client.html#ga21056ae0ce5d1b29c682641088d4af57", null ],
    [ "httpclient_put", "group___http_client.html#ga0b4b30d83daebddbc501a4694108fa66", null ],
    [ "httpclient_recv_response", "group___http_client.html#ga13891367f8a2a8d562083fc2bd46b066", null ],
    [ "httpclient_send_request", "group___http_client.html#ga079b58ff53ec99d18245ee4eeee211b7", null ],
    [ "httpclient_set_custom_header", "group___http_client.html#ga1d1e5926880a73aed4188a61672a1c6c", null ]
];